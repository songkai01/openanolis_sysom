from django.db import models
from lib.base_model import BaseModel
from django.contrib.auth import get_user_model

# Hotfix sys_hotfix Design
# for building_status->0:waiting; 1:building 2:build failed 3:build success
# for hotfix_necessary->0:optional 1:recommand install 2:must install
# for hotfix_risk -> 0:low risk 1:mid risk 2:high risk
class HotfixModel(BaseModel):
    arch = models.CharField(max_length=10, verbose_name="架构")
    os_type = models.CharField(max_length=50, default="Anolis", verbose_name="操作系统类型")
    kernel_version = models.CharField(max_length=60, verbose_name="内核版本")
    patch_path = models.CharField(max_length=255, verbose_name="补丁路径")
    patch_file = models.CharField(max_length=255, default="patch", verbose_name="patch名称")
    hotfix_path = models.CharField(max_length=255, verbose_name="rpm存储路径")
    hotfix_name = models.CharField(max_length=255, default="hotfix", verbose_name="补丁名称")
    building_status = models.IntegerField(default=0, verbose_name="构建状态")
    hotfix_necessary = models.IntegerField(default=0, verbose_name="补丁重要性")
    hotfix_risk = models.IntegerField(default=0, verbose_name="补丁风险")
    description = models.CharField(max_length=300, default="NULL", verbose_name="描述")
    log = models.TextField(default="", verbose_name="构建日志")
    log_file = models.CharField(max_length=255, verbose_name="日志存储路径")
    creator = models.CharField(max_length=20, default="admin", verbose_name="创建者")
    formal = models.BooleanField(default=0, verbose_name="正式包")
    rpm_package = models.CharField(max_length=255, verbose_name="rpm包名")

    class Meta:
        db_table = 'sys_hotfix'
        ordering = ['-created_at']

    def __str__(self):
        return self.patch_path

# This table is used to record the os_type to git location
class OSTypeModel(BaseModel):
    os_type = models.CharField(max_length=25, verbose_name="操作系统类型")
    source_repo = models.CharField(default="",max_length=255, verbose_name="源码仓库地址")
    image = models.CharField(max_length=255, verbose_name="该系列的构建容器镜像")
    git_rule = models.CharField(max_length=255, default="", verbose_name="该系列的源码git正则规则")
    source_devel = models.CharField(max_length=255, default="", verbose_name="该系列的devel仓库地址")
    source_debuginfo = models.CharField(max_length=255, default="", verbose_name="该系列的debuginfo仓库地址")
    sync_status = models.IntegerField(null=True, blank=True, verbose_name="同步状态")
    src_pkg_mark = models.BooleanField(default=0, verbose_name="是否使用src包")
    synced_at = models.CharField(max_length=255,null=True, blank=True, verbose_name="同步时间")

    class Meta:
        db_table = "sys_hotfix_ostype"
        ordering = ['-created_at']

# This table is used to record the relationship of kernel version to the branch and package
class KernelVersionModel(BaseModel):
    kernel_version = models.CharField(max_length=60, verbose_name="内核版本")
    os_type = models.CharField(max_length=25, verbose_name="操作系统类型")
    source = models.CharField(max_length=255, default="", verbose_name="源码来源")
    devel_link = models.TextField(default="", verbose_name="devel包链接")
    debuginfo_link = models.TextField(default="", verbose_name="debuginfo包链接")
    image = models.CharField(default="",max_length=255, verbose_name="构建镜像")
    use_src_package = models.BooleanField(default="1", verbose_name="使用源码包")

    class Meta:
        db_table = "sys_hotfix_kernelversion"
        ordering = ['-created_at']


class ReleasedHotfixListModule(BaseModel):
    serious_choice = [
        (0, u'可选安装'),
        (1, u'建议安装'),
        (2, u'需要安装')
    ]
    system_choice = [
        (0, u'调度'),
        (1, u'内存'),
        (2, u'网络'),
        (3, u'存储'),
        (4, u'其他')
    ]
    deprecated_choice = [
        (0, u'正常'),
        (1, u'废弃')
    ]
    hotfix_id = models.CharField(max_length=50, verbose_name="hotfix id")
    released_kernel_version = models.CharField(max_length=180, verbose_name="hotfix发布的内核版本")
    serious = models.IntegerField(default=0, choices=serious_choice, verbose_name="hotfix推荐安装级别")
    serious_explain = models.TextField(default="", verbose_name='推荐说明', blank=True)
    description = models.TextField(default="", verbose_name="hotfix问题描述", blank=True)
    fix_system = models.IntegerField(default=0, choices=system_choice, verbose_name="涉及子系统")
    released_time = models.DateTimeField(auto_now=False, verbose_name="发布时间")
    download_link = models.TextField(default="", verbose_name="hotfix下载链接", blank=True)
    deprecated = models.IntegerField(default=0, choices=deprecated_choice, verbose_name="该hotfix是否被废弃")
    deprecated_info = models.TextField(default="", verbose_name="废弃原因或信息", blank=True)
    modified_time = models.DateTimeField(auto_now=True, verbose_name='记录修改时间')
    modified_user = models.CharField(default="", max_length=20, null=True, verbose_name="用于记录最后一次修改的人")

    class Meta:
        db_table = "sys_released_hotfix"
        ordering = ['-created_at']