# -*- coding: utf-8 -*- #
"""
Time                2022/11/14 14:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                schemas.py
Description:
"""
from pydantic import BaseModel
from datetime import datetime 

###########################################################################
# Define schemas here
###########################################################################

class Knowledge(BaseModel):
    create_time: datetime
    issuedesc: str
    calltrace: str
    calltrace_extrack: str
    logs: str
    funcs: str
    issuelink: str
    field: str
    diagret: str

    class Config:
        orm_mode = True

class KnowledgeRecord(BaseModel):
    recordtime: datetime
    source: str
    calltrace: str
    logs: str
    funcs: str
    knowledge_ret: str

    class Config:
        orm_mode = True
