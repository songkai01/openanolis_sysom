"""
Time                2023/07/17 17:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                command.py
Description:
"""
from .base import DiagnosisPreProcessor, DiagnosisTask, DiagnosisJob


class PreProcessor(DiagnosisPreProcessor):
    """Command diagnosis

    Just invoke command in target instance and get stdout result

    Args:
        DiagnosisPreProcessor (_type_): _description_
    """

    def get_diagnosis_cmds(self, params: dict) -> DiagnosisTask:
        value = params.get("value", "")
        instance = params.get("instance", "")
        return DiagnosisTask(
            jobs=[DiagnosisJob(instance=instance, cmd="")],
            offline_mode=True,
            offline_results=[
                value
            ]
        )
