"""
Time                2023/06/19 17:32
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                command.py
Description:
"""
import json
from apps.task.models import JobModel
from clogger import logger
from .base import HookProcessResult, DiagnosisHookProcessor

class HookProcessor(DiagnosisHookProcessor):
    """Hook-processor used to invoke hook scripts

    Args:
        DiagnosisProcessorBase (_type_): _description_
    """
    
    async def invoke_hook(self, instance: JobModel, params: dict) -> HookProcessResult:
        """Invoke hook scripts

        Args:
            params (dict): Diagnosis parameters

        Returns:
            HookProcessResult: Hook process result
        """
        replace_text = params.get("replace_text", "")
        
        origin_result = json.loads(instance.result)
        origin_result["CommandResult"]["data"][0] = {"key": "", "value": replace_text}
        
        instance.result = json.dumps(origin_result)
        await self.save_job(instance)
        return HookProcessResult(code=200, data={}, err_msg="")