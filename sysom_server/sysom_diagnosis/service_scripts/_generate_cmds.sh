
# generate_subcmd() {
#     service_name=$1
#     subcmd=$2
#     echo ${service_name}
#     cat << EOF >> tmp_result
# sub_${serivce_name} $subcmd
# EOF
# }


# generate_subcmd filecache `python filecache '{"instance": "127.0.0.1", "value": 1}' | jq ".commands[0].cmd"`

# file cache
echo "filecache: " `python filecache '{"instance": "127.0.0.1", "value": 1}' | jq ".commands[0].cmd"`
# iofsstat
echo "iofsstat: " `python iofsstat '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# iohang
echo "iohang: " `python iohang '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# iolatency
echo "iolatency: " `python iolatency '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# iosdiag_latency
echo "iosdiag_latency: " `python iosdiag_latency '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# jitter
echo "jitter: " `python jitter '{"instance": "127.0.0.1", "time": 1}' | jq ".commands[0].cmd"`
# loadtask
echo "loadtask: " `python loadtask '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# memgraph
echo "memgraph: " `python memgraph '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# oomcheck
echo "oomcheck: " `python oomcheck '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# ossre
echo "ossre: " `python ossre '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# packetdrop
echo "packetdrop: " `python packetdrop '{"instance": "127.0.0.1", "time": 1}' | jq ".commands[0].cmd"`
# pingtrace
echo "pingtrace: " `python pingtrace '{"origin_instance": "127.0.0.1", "pkg_num": 1, "time_gap": 1, "target_instance": "192.168.0.22"}' | jq ".commands[0].cmd"` `python pingtrace '{"origin_instance": "127.0.0.1", "pkg_num": 1, "time_gap": 1, "target_instance": "192.168.0.22"}' | jq ".commands[1].cmd"`
# retran
echo "retran: " `python retran '{"instance": "127.0.0.1", "time": 1}' | jq ".commands[0].cmd"`
# schedmoni
echo "schedmoni: " `python schedmoni '{"instance": "127.0.0.1"}' | jq ".commands[0].cmd"`
# taskprofile
echo "taskprofile: " `python taskprofile '{"instance": "127.0.0.1", "timeout": 1}' | jq ".commands[0].cmd"`