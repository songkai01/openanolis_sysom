import time
from abc import ABC, abstractmethod
from dateutil import parser
from typing import List
from clogger import logger
from app.schemas import AlertData


class SourceConverterBase(ABC):
    @staticmethod
    def iso_to_timestamp(iso_time: str) -> int:
        try:
            res = int(parser.parse(iso_time).timestamp()) * 1000
            if res < 0:
                return 0
            else:
                return res
        except Exception as e:
            logger.exception(e)
            return 0

    @abstractmethod
    def convert(self, alert_source_type: str, alert_data: dict) -> List[AlertData]:
        pass

