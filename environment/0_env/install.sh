#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
ALIYUN_MIRROR="https://mirrors.aliyun.com/pypi/simple/"
VIRTUALENV_HOME=$GLOBAL_VIRTUALENV_HOME

if [ "$UID" -ne 0 ]; then
    echo "Please run as root"
    exit 1
fi

check_selinux_status() {
    ###check selinux rpm###
    rpm -qa | grep selinux-policy
    if [ $? -eq 0 ]; then
        cat /etc/selinux/config | grep "SELINUX=disabled"
        if [ $? -eq 0 ]; then
            echo "selinux disable..."
        else
            echo "selinux enable, please set selinux disable"
            exit 1
        fi
    else
        echo "selinux rpm package not install"
    fi
}

touch_env_rpms() {
    if [ -f /etc/alios-release ]; then
        if [ ! -f /etc/yum.repos.d/epel.repo ]; then
            wget -O /etc/yum.repos.d/epel.repo http://mirrors.aliyun.com/repo/epel-7.repo
        fi
    elif [ -f /etc/anolis-release ]; then
        sed -i '/epel\/8\/Everything/{n;s/enabled=0/enabled=1/;}' /etc/yum.repos.d/AnolisOS-DDE.repo
    fi
    command -v python3 || rpm -q --quiet python3 || yum install -y python3
    rpm -q --quiet wget || yum install -y wget
    rpm -q --quiet cronie || yum install -y cronie
}

check_requirements() {
    echo "INFO: begin install requirements..."
    # if ! [ -d ${SERVER_HOME}/logs/ ]; then
    #     mkdir -p ${SERVER_HOME}/logs/ || exit 1
    # fi

    local requirements_log="${LOG_HOME}/requirements.log"
    touch "$requirements_log" || exit
    ### atomic-0.7.3 need cffi, we show install cffi first###
    pip install --upgrade pip
    pip install cffi
    pip install -r requirements.txt -i "${ALIYUN_MIRROR}" | tee -a "${requirements_log}" || exit 1
    local pip_res=$?
    if [ $pip_res -ne 0 ]; then
        echo "ERROR: requirements not satisfied and auto install failed, please check ${requirements_log}"
        exit 1
    fi
}

touch_virtualenv() {
    if [ -d ${VIRTUALENV_HOME} ]; then
        echo "virtualenv exists, skip"
        echo "INFO: activate virtualenv..."
        source ${VIRTUALENV_HOME}/bin/activate || exit 1
    else
        mkdir -p ~/.pip
        cp pip.conf ~/.pip/
        cp .pydistutils.cfg ~/.pydistutils.cfg
        python3 -m venv ${VIRTUALENV_HOME}
        if [ "$?" != 0 ]; then
            pip3 install virtualenv
            python3 -m virtualenv ${VIRTUALENV_HOME}
        fi
        if [ "$?" = 0 ]; then
            echo "INFO: create virtualenv success"
        else
            echo "ERROR: create virtualenv failed"
            exit 1
        fi
        echo "INFO: activate virtualenv..."
        source ${VIRTUALENV_HOME}/bin/activate || exit 1
    fi
}

install_app() {
    check_selinux_status
    touch_env_rpms
    touch_virtualenv
    check_requirements
}

install_app
