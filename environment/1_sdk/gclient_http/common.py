# -*- coding: utf-8 -*- #
"""
Time                2022/7/29 13:33
Author:             mingfeng (SunnyQjm)
Email               mfeng@linux.alibaba.com
File                common.py
Description:
"""


class StaticConst:
    """Static consts

    This class defines all the static constant values in the cmg-redis module
    """
    # List of specialization parameters
    SPECIAL_PARM_TLS = \
        "tls"

    _special_parameter_list = [
        SPECIAL_PARM_TLS
    ]

    _special_parameters_default_value = {
        SPECIAL_PARM_TLS: (bool, False)
    }

    @classmethod
    def parse_special_parameter(cls, params: dict) -> dict:
        """Parse specialization parameters

        Parse the specialization parameters and remove the specialization
        parameters from the parameter list

        Args:
            params(dict): CecUrl.params

        Returns:

        """
        res = {}
        for key in cls._special_parameter_list:
            _type, default = \
                cls._special_parameters_default_value[key]
            res[key] = _type(params.pop(key, default))
        return res
