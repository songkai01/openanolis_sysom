#!/bin/bash
BaseDir=$(dirname $(readlink -f "$0"))
SERVICE_NAME=sysom-redis

clear_local_redis() {
    rm -rf /etc/supervisord.d/${SERVICE_NAME}.ini
    rm -rf ${DEPS_HOME}/redis
    ###use supervisorctl update to stop and clear services###
    supervisorctl update
}

clear_app() {
    ###we need redis version >= 5.0.0, check redis version###
    redis_version=`yum list all | grep "^redis.x86_64" | awk '{print $2}' | awk -F"." '{print $1}'`
    echo ${redis_version}
    if [ $redis_version -lt 5 ]
    then
        clear_local_redis
    else
        systemctl stop redis.service
    fi
}

# Stop first
bash -x $BaseDir/stop.sh

clear_app