import { PageContainer } from '@ant-design/pro-layout';
import React, { useState, useEffect } from 'react';
import { request } from 'umi';
import Dashboard from '../components/Dashboard';
import { getTask } from '../service'
import _ from "lodash";
import menu from '@/locales/en-US/menu';

const DiagnoseDetai = (props) => {
    const [pannelConfig, setPannelConfig] = useState({});
    const [data, setData] = useState();

    let taskId = props?.match?.params?.task_id;
    useEffect(async () => {
        if (!!taskId) {
            const taskResult = await getTask(taskId);
            const service_name = taskResult.service_name;

            const localesConfig = await request(`/resource/diagnose/v2/locales.json`)
            const menus = localesConfig.menus;
            let pannelJsonUrl = ""
            for (let i = 0; i < menus.length; i++) {
                let menu = menus[i];
                if (menu.endsWith(service_name)) {
                    pannelJsonUrl = `/resource/diagnose/v2${(menu.split("menu.diagnose")[1].split(".")).join("/")}.json`
                    break
                }
            }
            if (!pannelJsonUrl) {
                return
            }
            let targetPannelConfig = await request(pannelJsonUrl)
            setPannelConfig(targetPannelConfig)
            setData({
                ...taskResult,
                ...taskResult.result
            })
        }

    }, [])

    const refreshTask = async (record) => {
        const msg = await getTask(record.task_id);

        // window.open(`/diagnose/detail/${record.task_id}`)

        setData({ ...msg, ...msg.result });

    }

    return (
        <PageContainer title="诊断详情" subTitle={`(${taskId})`}>
            {
                data?.status == "Success" ?
                    data && <Dashboard
                        variables={pannelConfig.variables}
                        serviceName={pannelConfig.servername}
                        pannels={pannelConfig.pannels}
                        datas={data}
                        refreshTask={refreshTask}
                    />
                    :
                    <div>{data?.err_msg ? data.err_msg : data?.result}</div>
            }
        </PageContainer>
    );
};

export default DiagnoseDetai;
