import ProForm, { ProFormText, } from '@ant-design/pro-form';
import { Button } from 'antd';
import { useRequest, useIntl, FormattedMessage } from 'umi';
import ProCard from '@ant-design/pro-card';
import { postTask } from '../../service'

export default (props) => {
    const intl = useIntl();
    const { loading, error, run } = useRequest(postTask, {
        manual: true,
        onSuccess: (result, params) => {
            props?.onSuccess?.(result, params);
        },
    });

    return (

        <ProCard>
            <ProForm
                onFinish={async (values) => {
                    run(values)
                }}
                submitter={{
                    submitButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                    resetButtonProps: {
                        style: {
                            display: 'none',
                        },
                    },
                }}
                layout={"horizontal"}
                autoFocusFirstInput
            >
                <ProFormText
                    name={"service_name"}
                    initialValue={"loadtask"}
                    hidden={true}
                />
                <ProForm.Group>
                    <ProFormText
                        name={"instance"}
                        width="md"
                        label={intl.formatMessage({
                            id: 'pages.diagnose.instanceIP',
                            defaultMessage: 'Instance IP',
                        })}
                    />
                    <Button type="primary" htmlType="submit" loading={loading}><FormattedMessage id="pages.diagnose.startdiagnosis" defaultMessage="Start diagnosis" /></Button>
                </ProForm.Group>
            </ProForm>
        </ProCard>
    )
}
