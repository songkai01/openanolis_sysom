import styles from "./index.less";
import {
    ModalForm,
    ProForm,
    ProFormSelect,
} from "@ant-design/pro-components";
import { useIntl } from "umi";
import MenuIcon from '../../assets/menu.svg'
import EditIcon from '../../assets/edit.svg'
import DeleteIcon from '../../assets/cross.svg'
import { componentConfigs } from "../../component-config";
import DynamicForm from "./renderForm";
import { usePanelGeneratorStore } from "@/pages/diagnose/generate/Pannel/store/panelGeneratorStore";

/**
 * component toolbar
 * @param {number} displayIndex
 * @param {number} index
 * @param {Object} value
 * @param {Function} drag
 * @param {Function} preview
 * @returns {JSX.Element}
 * @constructor
 */
export const ComponentToolbar = ({ displayIndex, index, value, drag, preview, rowKey }) => {
    const configStore = usePanelGeneratorStore((state) => state.data)
    const setConfigStore = usePanelGeneratorStore((state) => state.setConfigStore)
    const intl = useIntl()
    const container = componentConfigs[value.type].container
    const deleteItem = (index) => {
        if (rowKey) {
            const currentPanel = configStore.pannels
            const currentRow = currentPanel.find((item) => item.key === rowKey)
            currentRow.children.splice(index, 1)
            setConfigStore({
                ...configStore,
                pannels: currentPanel.map((item) => {
                    if (item.key === rowKey) {
                        return currentRow
                    }
                    return item
                })
            })
        } else {
            if (container === 'taskform') {
                const currentForm = configStore.taskform
                currentForm.splice(index, 1)
                setConfigStore({ ...configStore, taskform: currentForm })
            }
            else if (container === 'panels') {
                const currentPanel = configStore.pannels
                currentPanel.splice(index, 1)
                setConfigStore({ ...configStore, pannels: currentPanel })
            }
        }
    }
    return (
        <div style={displayIndex === index ? {} : { display: 'none' }} className={styles.toolbar}>
            <div ref={drag} className={`${styles.item} ${styles.drag}`}
                style={{ background: `url(${MenuIcon}) center center`, backgroundSize: '90% 90%' }}>
            </div>
            {componentConfigs[value.type]?.editForm && (
                <ModalForm
                    title={intl.formatMessage({ id: 'pages.diagnose.generatePanel.editComponentParameters', defaultMessage: 'Edit component parameters' })}
                    trigger={
                        <div className={styles.item} style={{ background: `url(${EditIcon}) center center`, backgroundSize: '90% 90%' }}>
                        </div>
                    }
                    modalProps={{
                        destroyOnClose: true
                    }}
                    submitTimeout={2000}
                    onFinish={(values) => {
                        if (rowKey) {
                            const currentPanel = configStore.pannels
                            const currentRow = currentPanel.find((item) => item.key === rowKey)
                            currentRow.children = currentRow.children.map((item, i) => {
                                if (i === index) {
                                    return {
                                        ...item,
                                        ...values
                                    }
                                }
                                return item
                            })
                            setConfigStore({
                                ...configStore,
                                pannels: currentPanel.map((item) => {
                                    if (item.key === rowKey) {
                                        return currentRow
                                    }
                                    return item
                                })
                            })
                        } else {
                            if (container === 'taskform') {
                                setConfigStore({
                                    ...configStore,
                                    taskform: configStore.taskform.map((item, i) => {
                                        if (i === index) {
                                            return {
                                                ...item,
                                                ...values
                                            }
                                        }
                                        return item
                                    }
                                    ),
                                })
                            } else if (container === 'panels') {
                                setConfigStore({
                                    ...configStore,
                                    pannels: configStore.pannels.map((item, i) => {
                                        if (i === index) {
                                            return {
                                                ...item,
                                                ...values
                                            }
                                        }
                                        return item
                                    }
                                    ),
                                })
                            }
                        }
                        return true
                    }}
                >
                    <ProForm.Group>
                        <ProFormSelect
                            options={Object.keys(componentConfigs).map((key) => {
                                return {
                                    label: componentConfigs[key].name,
                                    value: key
                                }
                            })}
                            width="md"
                            name="type"
                            label={intl.formatMessage({ id: 'pages.diagnose.generatePanel.componentType', defaultMessage: '组件类型' })}
                            disabled
                            initialValue={value.type}
                        />

                        {
                            componentConfigs[value.type].editForm.map((item, index) => {
                                return <DynamicForm item={item} value={value} key={index} />
                            })
                        }
                    </ProForm.Group>
                </ModalForm>
            )}
            <div className={styles.item} style={{ background: `url(${DeleteIcon}) center center`, backgroundSize: '90% 90%' }}
                onClick={() => {
                    deleteItem(index)
                }}>
            </div>
        </div>
    )
}

