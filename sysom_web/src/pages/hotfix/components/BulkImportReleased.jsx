/*
 * @Author: wb-msm241621
 * @Date: 2023-12-15 17:19:58
 * @LastEditTime: 2024-01-02 15:04:05
 * @Description: 
 */
import { Button, Row, Col, message } from 'antd';
import { ModalForm, ProFormUploadDragger } from '@ant-design/pro-form'
import { ImportOutlined } from "@ant-design/icons";
import { useIntl, FormattedMessage } from 'umi';


const BulkImportReleased = (props) => {
    const { uploadHandler, templateUrl, successCallBack } = props
    const intl = useIntl();

    const bulkImportUploadHandler = async (rawfile) => {
        return new Promise( async (resolve, reject) => {
            const formData = new FormData();
            formData.append("file", rawfile);
            await uploadHandler(formData).then((result) => {
                resolve(result)
                message.success("upload file success")
            }).catch((e) => {
                reject(e)
            })
        })
    }

    return <ModalForm
        title={intl.formatMessage({
            id: 'pages.hotfix.bulk_import_hotfix_released',
            defaultMessage: 'Batch import hotfix released',
        })}
        trigger={<Button type='primary' icon={<ImportOutlined />}>
            <FormattedMessage id="pages.hostTable.batchimport" defaultMessage="batch import" />
        </Button>}
        onFinish={ async (values) => {
            let state = true
            const fileObj = values.file[0].originFileObj
            await bulkImportUploadHandler(fileObj).then((result) => {
                successCallBack()
            }).catch((e) => {
                state = false
            })
            return state
        }}
    >
        <Row align="middle" justify="space-around">
            <Col span={16}>
                <ProFormUploadDragger
                    accept='.xls, .xlsx'
                    name="file"
                    fieldProps={{
                        beforeUpload: () => { return false }
                    }}
                />
            </Col>
            <Col span={4}>
                <Button type="link" href={templateUrl}>
                    <FormattedMessage id="pages.hostTable.temdownload" defaultMessage="Template download" />
                </Button>
            </Col>
        </Row>

    </ModalForm>
}

export default BulkImportReleased;
