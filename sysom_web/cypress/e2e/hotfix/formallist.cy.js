/* ==== Test Created with Cypress Studio ==== */
/**Before this test case begins, there should be one successfully built. 
And this hotfix should be changed into formal, so that there is a record 
for formal hotfix test */

it('formal hotfix test', function() {
    cy.login()
    /* ==== Generated with Cypress Studio ==== */
    cy.visit('/hotfix/formal_hotfix');
    cy.get('#created_at').click();
    cy.get('.ant-picker-today-btn').click();
    cy.get(':nth-child(1) > .ant-btn > span').click({multiple: true});
    cy.get('#hotfix_name').clear('te');
    cy.get('#hotfix_name').type('test');
    cy.get(':nth-child(2) > .ant-btn > span').click();
    cy.get('.ant-btn > .anticon > svg').click();
    cy.get('.ant-space > :nth-child(2) > span > a').click();
    /* ==== End Cypress Studio ==== */
  });