/// <reference types="cypress" />

describe("SysOM Migration Monitor Dashboard Test", () => {
    beforeEach(() => {
        cy.login()
    })

    it("Migration monitor test", () => {
        // 1. 访问java可观测
        cy.visit("/app_observable/process_app");

        // 2. 等待页面加载完成
        cy.wait(1000);

        // 运行时间（Stat面板数值类型）
        cy.getPannelContentByTitle("运行时间")
            .then(($el) => {
            if ($el.text().includes("No data")) {
                // 面板没有数据的情况
                cy.wrap($el).contains("No data");
            } 
            else {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
            }
        });

        // 虚拟内存（Stat面板数值类型）
        cy.getPannelContentByTitle("虚拟内存")
            .then(($el) => {
            if ($el.text().includes("No data")) {
                // 面板没有数据的情况
                cy.wrap($el).contains("No data");
            } 
            else {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
            }
        });

        // 最大时延（Stat面板数值类型）
        cy.getPannelContentByTitle("最大延时")
            .then(($el) => {
            if ($el.text().includes("No data")) {
                // 面板没有数据的情况
                cy.wrap($el).contains("No data");
            } 
            else {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
            }
        });

        // cpu（Pie Chart）
        cy.getPannelContentByTitle("cpu").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        // 内存（Pie Chart）
        cy.getPannelContentByTitle("内存").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        // cpu占用率（Pie Chart）
        cy.getPannelContentByTitle("cpu占用率").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列


        // IO 吞吐量（Time series 面板）
        cy.getPannelContentByTitle("IO 吞吐量").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("IO 吞吐量").find("tbody tr").eq(0).find("td").eq(0).contains("read_bytes");  // 第一列的第一行是 read_bytes
        cy.getPannelContentByTitle("IO 吞吐量").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });
        cy.getPannelContentByTitle("IO 吞吐量").find("tbody tr").eq(1).find("td").eq(0).contains("write_bytes");  // 第一列的第二行是 write_bytes
        cy.getPannelContentByTitle("IO 吞吐量").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });


        // cpu占用率（Time series 面板）
        cy.getPannelContentByTitle("cpu占用率折线图").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("cpu占用率折线图").find("tbody tr").eq(0).find("td").eq(0).contains("内核态cpu_sys");  // 第一列的第一行是 内核态cpu_sys
        cy.getPannelContentByTitle("cpu占用率折线图").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });
        cy.getPannelContentByTitle("cpu占用率折线图").find("tbody tr").eq(1).find("td").eq(0).contains("用户态cpu_user");  // 第一列的第二行是 用户态cpu_user
        cy.getPannelContentByTitle("cpu占用率折线图").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });


        // 进程主次缺页次数（Time series 面板）
        cy.getPannelContentByTitle("进程主次缺页次数").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("进程主次缺页次数").find("tbody tr").eq(0).find("td").eq(0).contains("主缺页次数majflt");  // 第一列的第一行是 主缺页次数majflt
        cy.getPannelContentByTitle("进程主次缺页次数").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });
        cy.getPannelContentByTitle("进程主次缺页次数").find("tbody tr").eq(1).find("td").eq(0).contains("次缺页次数minflt");  // 第一列的第二行是 次缺页次数minflt
        cy.getPannelContentByTitle("进程主次缺页次数").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });


        // IO延迟（Time series 面板）
        cy.getPannelContentByTitle("IO延迟").find("tbody tr").should("have.length", 1);   // Legend 有两列
        cy.getPannelContentByTitle("IO延迟").find("tbody tr").eq(0).find("td").eq(0).contains("IO延迟");  // 第一列的第一行是 IO延迟
        cy.getPannelContentByTitle("IO延迟").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });


        // 进程切换（Time series 面板）
        cy.getPannelContentByTitle("进程切换").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("进程切换").find("tbody tr").eq(0).find("td").eq(0).contains("进程自主切换");  // 第一列的第一行是 进程自主切换
        cy.getPannelContentByTitle("进程切换").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });
        cy.getPannelContentByTitle("进程切换").find("tbody tr").eq(1).find("td").eq(0).contains("进程非自主切换");  // 第一列的第二行是 进程非自主切换
        cy.getPannelContentByTitle("进程切换").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });


        // 进程调度状态（Time series 面板）
        cy.getPannelContentByTitle("进程调度状态").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("进程调度状态").find("tbody tr").eq(0).find("td").eq(0).contains("运行时间time");  // 第一列的第一行是 运行时间time
        cy.getPannelContentByTitle("进程调度状态").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });
        cy.getPannelContentByTitle("进程调度状态").find("tbody tr").eq(1).find("td").eq(0).contains("调度延迟delay");  // 第一列的第二行是 调度延迟delay
        cy.getPannelContentByTitle("进程调度状态").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.least(0);
        });

    })
})