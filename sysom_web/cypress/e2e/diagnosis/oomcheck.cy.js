/// <reference types="cypress" />

describe("SysOM Diagnosis Test -- oomcheck", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it("Invoke oomcheck diagnosis, and check result", () => {
        //cy.wait(5000)
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/memory/oomcheck",

            // 诊断参数
            {
                "instance": "127.0.0.1"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            /*{
                "id": 1601,
                "created_at": "2023-09-20 15:14:41",
                "updated_at": "2023-09-20 15:14:41",
                "task_id": "FArPtvj7",
                "status": "Success",
                "service_name": "oomcheck",
                "code": 0,
                "err_msg": "",
                "result": {
                    "oomResult": {
                        "data": [
                            {
                                "key": "OOM Tasks",
                                "value": "-"
                            },
                            {
                                "key": "The number of OOM ",
                                "value": "-"
                            },
                            {
                                "key": "OOM Type",
                                "value": "-"
                            }
                        ]
                    },
                    "oomAnalysis": {
                        "data": [
                            {
                                "key": "OOM root cause",
                                "value": "-"
                            },
                            {
                                "key": "OOM suggestion",
                                "value": "-"
                            }
                        ]
                    },
                    "oomDetail": {
                        "data": [
                            {
                                "key": 0,
                                "Free": "-",
                                "Low watermark": "-",
                                "RSS": "-"
                            }
                        ]
                    },
                    "oomTask": {
                        "data": [
                            {
                                "key": 0,
                                "Tasks": "-",
                                "Used": "-"
                            }
                        ]
                    }
                },
                "params": {
                    "service_name": "oomcheck",
                    "instance": "192.168.0.130",
                    "time": ""
                },
                    "created_by": 1,
                    "url": "/diagnose/detail/FArPtvj7"
            }*/
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                ///* ==== Generated with Cypress Studio ==== */
                cy.get('table > tbody').eq(1).children().should('have.length',1)
                cy.get(':nth-child(2) > [style="padding: 0px;"] > :nth-child(1) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/-|\S+\(\d+\)*/);
                cy.get(':nth-child(2) > [style="padding: 0px;"] > :nth-child(2) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/-|[0-9]*/);
                cy.get(':nth-child(2) > [style="padding: 0px;"] > :nth-child(3) > .ant-pro-card > .ant-pro-card-body > .ant-statistic > .ant-statistic-content > .ant-statistic-content-value').invoke('text').should('match',/-|OOM/);
                /* ==== End Cypress Studio ==== */
            })
    })
})
