/// <reference types="cypress" />

describe("SysOM Cluster Manager Test", () => {
    beforeEach(() => {
        // 自动登录
        cy.login()
    })
    it.only("Invoke command diagnosis, and check result", () => {
        cy.sysomDiagnosisCheck(
            // 诊断前端url
            "/diagnose/custom/command",

            // 诊断参数
            {
                "instance": "127.0.0.1",
                "command": "ls -ltrh"
            },

            // 诊断结果处理（在此处判断诊断的结果数据是否符合预期）
            // {
            //     "id": 83,
            //     "created_at": "2023-09-11 17:34:59",
            //     "updated_at": "2023-09-11 17:34:59",
            //     "task_id": "d4KvXgub",
            //     "status": "Success",
            //     "service_name": "command",
            //     "code": 0,
            //     "err_msg": "",
            //     "result": {
            //         "CommandResult": {
            //             "data": [
            //                 {
            //                     "key": "",
            //                     "value": "total 600M\n-rw-r--r--  1 root root  45M Jun 26 22:17 sysom.tar.gz\ndrwxr-xr-x 14 root root 4.0K Aug"
            //                 }
            //             ]
            //         }
            //     },
            //     "params": {
            //         "service_name": "command",
            //         "instance": "127.0.0.1",
            //         "command": "ls -ltrh"
            //     },
            //     "created_by": 1,
            //     "url": "/diagnose/detail/d4KvXgub"
            // }
            (result) => {
                // result => 包含诊断API返回的诊断详情数据

                ////////////////////////////////////////////////////////////
                // 在此处补充诊断详情渲染后的前端页面是否符合预期
                // 断言文档：https://docs.cypress.io/guides/references/assertions#Text-Content
                ////////////////////////////////////////////////////////////
                /* ==== Generated with Cypress Studio ==== */
                cy.get('.ant-statistic-content-value').should("contain.text", "total")
                cy.get('.ant-statistic-content-value').should(($element) => {
                    // total 600M
                    // -rw-r--r--  1 root root  45M Jun 26 22:17 sysom.tar.gz
                    // drwxr-xr-x 14 root root 4.0K Aug  2 11:36 sysom-2.2
                    // -rw-r--r--  1 root root 238M Aug  2 11:38 sysom-2.2.tar.gz
                    // drwxr-xr-x 14 root root 4.0K Aug  2 14:53 sysom-3.0
                    // drwxr-xr-x  8 root root 4.0K Aug  2 14:55 rpmbuild
                    // -rw-r--r--  1 root root 238M Aug  2 14:56 sysom-3.0.tar.gz
                    // drwxr-xr-x 14 root root 4.0K Sep 11 20:31 sysom
                    let lines = $element.text().split("\n")

                    // 断言结果文本至少大于 1 行
                    expect(lines.length).to.be.gt(1)

                    // total 600M
                    let first_line = lines[0]
                    first_line = first_line.replace("total ", "")

                    // 断言总文件大小至少大于0
                    expect(parseInt(first_line)).to.be.gt(0)
                })
                cy.get('.ant-pro-card-border > .ant-pro-card-header > .ant-pro-card-title').should("contain.text", "Command Result")
                /* ==== End Cypress Studio ==== */
            })
    })
})