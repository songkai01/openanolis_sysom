/// <reference types="cypress" />

describe("SysOM Vul Manager Test", () => {
    beforeEach(() => {
        cy.login()
    })
    it("repair test", () => {

        cy.intercept("POST", "api/v1/vul/")
            .as("repair")

        // 1. 访问安全中心列表
        cy.visit("/security/list")

        //2.选择第一条数据，点击修复按钮
        cy.get('table').within(() => {
            cy.get('tr').eq(1).contains("修复").click();
        })

        //3.点击全选
        cy.get('.ant-checkbox-wrapper > .ant-checkbox > .ant-checkbox-input').check();

        //4.点击一键修复
        cy.get("button").contains("一键修复").click()

        //5.文件结果判断
        cy.wait('@repair')
            .then((interception) => {
                cy.wrap({
                    statusCode: interception.response?.statusCode
                }).its("statusCode").should("eq", 200)
            })

        //6.漏洞修复后的OK按钮点击
        cy.get('.ant-modal-confirm-btns > .ant-btn').click();
    }
    )
})