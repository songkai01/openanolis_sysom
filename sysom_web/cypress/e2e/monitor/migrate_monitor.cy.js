/// <reference types="cypress" />

describe("SysOM Migration Monitor Dashboard Test", () => {
    beforeEach(() => {
        cy.login()
    })

    it("Migration monitor test", () => {
        // 1. 访问主机列表也米娜
        cy.visit("/monitor/migration");

        // 2. 等待页面加载完成
        cy.wait(2000);

        // 当前内核版本（Stat面板数值类型）
        cy.getPannelContentByTitle("当前内核版本").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 当前内存可用总量（Stat面板数值类型）
        cy.getPannelContentByTitle("当前内存可用总量").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 当前大页内存总量（Stat面板数值类型）
        cy.getPannelContentByTitle("当前大页内存总量").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 当前磁盘可用空间总量（Stat面板数值类型）
        cy.getPannelContentByTitle("当前磁盘可用空间总量").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 当前磁盘个数（Stat面板数值类型）
        cy.getPannelContentByTitle("当前磁盘个数").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 当前网卡数量（Stat面板数值类型）
        cy.getPannelContentByTitle("当前网卡数量").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 当前启用网卡数量（Stat面板数值类型）
        cy.getPannelContentByTitle("当前启用网卡数量").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 可用内存（Time series 面板）
        cy.getPannelContentByTitle("可用内存").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("可用内存").find("tbody tr").eq(0).find("td").eq(0).contains("total");  // 第一列的第一行是 total
        cy.getPannelContentByTitle("可用内存").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });
        cy.getPannelContentByTitle("可用内存").find("tbody tr").eq(1).find("td").eq(0).contains("free");  // 第一列的第二行是 free
        cy.getPannelContentByTitle("可用内存").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 可用磁盘空间（Time series 面板）
        cy.getPannelContentByTitle("可用磁盘空间").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("可用磁盘空间").find("tbody tr").eq(0).find("td").eq(0).contains("total");  // 第一列的第一行是 total
        cy.getPannelContentByTitle("可用磁盘空间").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });
        cy.getPannelContentByTitle("可用磁盘空间").find("tbody tr").eq(1).find("td").eq(0).contains("avaliable");  // 第一列的第二行是 avaliable
        cy.getPannelContentByTitle("可用磁盘空间").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // CPUs（Stat面板数值类型）
        cy.getPannelContentByTitle("CPUs").contains(/\d+/).then(($el) => {
            const num = parseInt($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // CPU 利用率（Time series 面板）
        cy.getPannelContentByTitle("CPU 利用率").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列
        cy.getPannelContentByTitle("CPU 利用率").find("tbody tr").eq(0).find("td").eq(0).contains("今日");  // 第一列的第一行是 “今日”
        cy.getPannelContentByTitle("CPU 利用率").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // CPU 利用率两日差值（Time series 面板）=> 如果无数据也是正常的，可以用下列这种方式分情况判断
        cy.getPannelContentByTitle("CPU 利用率两日差值")
            .then(($el) => {
                if ($el.text().includes("No data")) {
                    // 面板没有数据的情况
                    cy.wrap($el).contains("No data");
                } else {
                    // 如果面板有数据的情况
                    cy.wrap($el).find("tbody tr").should("have.length.gte", 1);   // Legend 至少有1列
                    cy.wrap($el).find("tbody tr").eq(0).find("td").eq(0).contains("CPU 利用率差值");  // 第一列的第一行是 “CPU 利用率差值”
                    cy.wrap($el).find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
                        const num = parseFloat($el.text());
                        expect(num).to.be.greaterThan(0);
                    });
                    // cy.wrap($el).find("tbody tr").eq(1).find("td").eq(0).contains("stdvar");  // 第二列的第一行是 “CPU 利用率差值”
                    // cy.wrap($el).find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
                    //     const num = parseFloat($el.text());
                    //     expect(num).to.be.gte(0);
                    // });
                }
            });
        
        // 实时 CPU 利用率（Gauge）
        cy.getPannelContentByTitle("实时 CPU 利用率").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // CPU 利用率两日分布（Histogram）
        cy.getPannelContentByTitle("CPU 利用率两日分布").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列
        cy.getPannelContentByTitle("CPU 利用率两日分布").find("tbody tr").eq(0).find("td").eq(0).contains("今日");  // 第一列的第一行是 “今日”
        cy.getPannelContentByTitle("CPU 利用率两日分布").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // CPU利用率波动（Time series 面板）
        cy.getPannelContentByTitle("CPU利用率波动").find("tbody tr").should("have.length", 1);   // Legend 有一列
        cy.getPannelContentByTitle("CPU利用率波动").find("tbody tr").eq(0).find("td").eq(0).contains("标准差");  // 第一列的第一行是 “标准差”
        cy.getPannelContentByTitle("CPU利用率波动").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 实时内存使用率（Gauge）
        cy.getPannelContentByTitle("实时内存使用率").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 内存使用率（Memory Usage）（Time series 面板）
        cy.getPannelContentByTitle("内存使用率（Memory Usage）").find("tbody tr").should("have.length", 1);   // Legend 有一列
        cy.getPannelContentByTitle("内存使用率（Memory Usage）").find("tbody tr").eq(0).find("td").eq(0).contains("Memory utilization rate");  // 第一列的第一行是 “Memory utilization rate”
        cy.getPannelContentByTitle("内存使用率（Memory Usage）").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.greaterThan(0);
        });

        // 内存使用率波动（Memory Usage Rate Fluctuation）（Time series 面板）
        cy.getPannelContentByTitle("内存使用率波动（Memory Usage Rate Fluctuation）").find("tbody tr").should("have.length", 1);   // Legend 有一列
        cy.getPannelContentByTitle("内存使用率波动（Memory Usage Rate Fluctuation）").find("tbody tr").eq(0).find("td").eq(0).contains("标准差(stddev)");  // 第一列的第一行是 “标准差”
        cy.getPannelContentByTitle("内存使用率波动（Memory Usage Rate Fluctuation）").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 磁盘空间总额（Pie Chart）
        cy.getPannelContentByTitle("磁盘空间总额").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        // 磁盘空间使用率（Time series 面板）
        cy.getPannelContentByTitle("磁盘空间使用率").find("ul li").should("have.length.gte", 1);   // Legend 至少有一列

        // Disk Read IOps （Time series 面板）
        cy.getPannelContentByTitle("Disk Read IOps").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        // Disk R/W Data （Time series 面板）
        cy.getPannelContentByTitle("Disk R/W Data").find("tbody tr").should("have.length.gte", 1);   // Legend 至少有一列

        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Network
        //////////////////////////////////////////////////////////////////////////////////////////////////

        // 实时接收速率（Stat面板）
        cy.getPannelContentByTitle("实时接收速率").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 网络流量监控
        cy.getPannelContentByTitle("网络流量监控").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("网络流量监控").find("tbody tr").eq(0).find("td").eq(0).contains("接收速率");  // 第一列的第一行是 “接收速率”
        cy.getPannelContentByTitle("网络流量监控").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });
        cy.getPannelContentByTitle("网络流量监控").find("tbody tr").eq(1).find("td").eq(0).contains("发送速率");  // 第二列的第一行是 “发送速率”
        cy.getPannelContentByTitle("网络流量监控").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.lte(0);
        });

        // 网络流量波动监测
        cy.getPannelContentByTitle("网络流量波动监测").find("tbody tr").should("have.length", 2);   // Legend 有两列
        cy.getPannelContentByTitle("网络流量波动监测").find("tbody tr").eq(0).find("td").eq(0).contains("接收速率标准差");  // 第一列的第一行是 “接收速率标准差”
        cy.getPannelContentByTitle("网络流量波动监测").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });
        cy.getPannelContentByTitle("网络流量波动监测").find("tbody tr").eq(1).find("td").eq(0).contains("发送速率标准差");  // 第二列的第一行是 “发送速率标准差”
        cy.getPannelContentByTitle("网络流量波动监测").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.lte(0);
        });

        // 实时发送速率（Stat面板）
        cy.getPannelContentByTitle("实时发送速率").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        //////////////////////////////////////////////////////////////////////////////////////////////////
        // System Load
        //////////////////////////////////////////////////////////////////////////////////////////////////

        // 实时系统负载（1分钟）（Gauge）
        cy.getPannelContentByTitle("实时系统负载（1分钟）").contains(/\d+/).then(($el) => {
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 系统负载（Time series 面板）
        cy.getPannelContentByTitle("系统负载").find("tbody tr").should("have.length", 3);   // Legend 有三列
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(0).find("td").eq(0).contains("1分钟内负载（Load1）");  // 第一列的第一行是 "1分钟内负载（Load1）"
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第一列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(1).find("td").eq(0).contains("5分钟内负载（Load5）");  // 第二列的第一行是 "5分钟内负载（Load5）"
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(1).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(2).find("td").eq(0).contains("15分钟内负载（Load15）");  // 第三列的第一行是 "15分钟内负载（Load15）"
        cy.getPannelContentByTitle("系统负载").find("tbody tr").eq(2).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第三列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });

        // 系统负载波动（Load1）（Time series 面板）
        cy.getPannelContentByTitle("系统负载波动（Load1）").find("tbody tr").should("have.length", 1);   // Legend 有一列
        cy.getPannelContentByTitle("系统负载波动（Load1）").find("tbody tr").eq(0).find("td").eq(0).contains("标准差");  // 第一列的第一行是 “标准差”
        cy.getPannelContentByTitle("系统负载波动（Load1）").find("tbody tr").eq(0).find("td").eq(1).contains(/\d+/).then(($el) => {  // 第二列的第二行是数值
            const num = parseFloat($el.text());
            expect(num).to.be.gte(0);
        });
    })
})